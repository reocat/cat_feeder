#include <ArduinoIoTCloud.h>
#include <Arduino_ConnectionHandler.h>

#define SECRET_SSID "Redmi 3"
#define SECRET_PASS "reocatwf"
#define SECRET_DEVICE_KEY "5ELZIQRCPHWHVPVYRPJF"

const char DEVICE_LOGIN_NAME[]  = "64a6c411-73a1-48c2-bb3d-01cf6b0759b7";

const char SSID[] = SECRET_SSID; // Network SSID (name)
const char PASS[] = SECRET_PASS; // Network password (use for WPA, or use as key for WEP)
const char DEVICE_KEY[] = SECRET_DEVICE_KEY; // Secret device password

void onStepsChange();
void onBackStepsChange();
void onFeedChange();
void onScheduleChange();

int steps;
int backSteps;
bool feed;
CloudSchedule schedule;

void initProperties() {
  ArduinoCloud.setBoardId(DEVICE_LOGIN_NAME);
  ArduinoCloud.setSecretDeviceKey(DEVICE_KEY);
  ArduinoCloud.addProperty(steps, READWRITE, ON_CHANGE, onStepsChange);
  ArduinoCloud.addProperty(backSteps, READWRITE, ON_CHANGE, onBackStepsChange);
  ArduinoCloud.addProperty(feed, READWRITE, ON_CHANGE, onFeedChange);
  ArduinoCloud.addProperty(schedule, READWRITE, ON_CHANGE, onScheduleChange);
}

WiFiConnectionHandler ArduinoIoTPreferredConnection(SSID, PASS);
