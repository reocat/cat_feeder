#include <SoftwareSerial.h>
#include "thingProperties.h"

#define SRX D5
#define STX D6

SoftwareSerial mySerial(SRX, STX);

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600);
  Serial.setDebugOutput(true);
  initProperties();
  ArduinoCloud.begin(ArduinoIoTPreferredConnection);
  setDebugMessageLevel(2);
  ArduinoCloud.printDebugInfo();
}


void loop() {
  int flag = 0;
  ArduinoCloud.update();
  if (schedule.isActive()) {
    String str = "f1";
    flag = flag + 1;
    if (flag <= 1) {
      Serial.println(str);
      mySerial.println("");
      mySerial.println(str);
      Serial.println(flag);
    }
    else {
      delay(2000);
      flag = 0;
    }
    /*
      Serial.println(str);
      mySerial.println("");
      mySerial.println(str);*/
  }
}

void onFeedChange() {
  Serial.println(feed);
  if (feed == 1) {
    String str = "f1";
    mySerial.println(str);
  }
}

void onStepsChange() {
  Serial.println(steps);
  String str = "sa" + String(steps);
  mySerial.println(str);
}

void onBackStepsChange() {
  Serial.println(backSteps);
  String str = "sb" + String(backSteps);
  mySerial.println(str);
}

void onScheduleChange() {
  /*if (schedule.isActive()) {
    Serial.println("eeeeee");
    }*/
}
