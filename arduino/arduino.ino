#include <SoftwareSerial.h>
#include <AccelStepper.h>

#define SRX 5
#define STX 6

#define DRIVER_STEPPER_MOT_SLP 2
#define STEPS_PER_REVOLUTION_IN_START 500
#define STEPS_PER_REVOLUTION_REV_IN_START 75

int stepsPerRevolution = STEPS_PER_REVOLUTION_IN_START;
int stepsPerRevolutionRev = STEPS_PER_REVOLUTION_REV_IN_START;

SoftwareSerial mySerial(SRX, STX);

AccelStepper myStepper(AccelStepper::DRIVER, 9, 8);

void setup() {
  Serial.begin(9600); // Initialize the serial port
  mySerial.begin(9600);
  myStepper.setMaxSpeed(500);
  myStepper.setAcceleration (100000);
  myStepper.setEnablePin(DRIVER_STEPPER_MOT_SLP);
  myStepper.disableOutputs();
}

void loop() {
  if (mySerial.available() > 2) {
    String input = mySerial.readStringUntil('\n');
    Serial.println(input);
    input.trim();
    input.replace(" ", ""); // Убрать возможные пробелы между символами
    byte strIndex = input.length(); // Переменая для хронения индекса вхождения цифры в входной строке, изначально равна размеру строки
    // Поиск первого вхождения цифры от 0 по 9 в подстроку
    for (byte i = 0; i < 10; i++) {
      byte index = input.indexOf(String(i));
      if (index < strIndex && index != 255) strIndex = index;
    }
    String key = input.substring(0, strIndex);
    int value = input.substring(strIndex, input.length()).toInt();
    if (key == "f") {
      Feed();
    } else if (key == "sa") {
      stepsPerRevolution = value;
      Serial.println(stepsPerRevolution);
    } else if (key == "sb") {
      stepsPerRevolutionRev = value;
      Serial.println(stepsPerRevolutionRev);
    }
    Serial.print(key);
    Serial.print(" = ");
    Serial.println(value);
  }
}

void Feed() {
  myStepper.enableOutputs();
  myStepper.move(stepsPerRevolution);
  int tmpSteps = myStepper.currentPosition() + stepsPerRevolution;
  while (myStepper.currentPosition() != tmpSteps) {
    myStepper.run();
  }
  myStepper.stop(); // Stop as fast as possible: sets new target
  myStepper.runToPosition(); // Now stopped after quickstop
  tmpSteps = myStepper.currentPosition() - stepsPerRevolutionRev;
  myStepper.move(-stepsPerRevolutionRev); // Now go backwards
  while (myStepper.currentPosition() != tmpSteps) {
    myStepper.run();
  }
  myStepper.stop(); // Stop as fast as possible: sets new target
  myStepper.runToPosition();
  myStepper.disableOutputs();
}
